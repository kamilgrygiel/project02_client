package bmiclient;


import bmiclient.GUI.PieChartSample;
import bmiclient.model.StatisticData;
import bmiclient.model.UserData;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.swing.*;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;

import static javafx.application.Application.launch;

public class Main {
    public static String firstName;
    public static String lastName;
    public static String weight;
    public static String height;
    public  static StatisticData statisticData = new StatisticData() ;
    public static void getServerBmiResponse() {
        final HttpClient client = HttpClientBuilder.create().build();


        final HttpGet request = new HttpGet("http://127.0.0.1:8080/bmi/chart");



        try {

            final HttpResponse response = client.execute(request);
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);

            String tempString;
            int k, l=0;
            for (int i = 0; i < json.length(); i++) {
                if (json.charAt(i) == ':') {
                    tempString = "";
                    k = i+2;
                        while (json.charAt(k) != '"'){

                            tempString = tempString + json.charAt(k);
                            k++;
                        }
                        i=k+1;
                    if (l==0) {
                        statisticData.setStarvation(tempString);
                        l++;
                    } else if (l==1) {
                        statisticData.setEmaciation(tempString);
                        l++;
                    } else if (l==2) {
                        statisticData.setUnderweight(tempString);
                        l++;
                    } else if (l==3) {
                        statisticData.setNormal(tempString);
                        l++;
                    } else if (l==4) {
                        statisticData.setOverweigt(tempString);
                        l++;
                    } else if (l==5) {
                        statisticData.setObesityI(tempString);
                        l++;
                    } else if (l==6) {
                        statisticData.setEmaciation(tempString);
                        l++;
                    } else if (l==7) {
                        statisticData.setEmaciation(tempString);
                        l++;
                    }

                }
                }





            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 200) {
                JOptionPane.showMessageDialog(null, "Odczytywanie JSON'a z serwera:");
                launch(PieChartSample.class);

            } else if(response.getStatusLine().getStatusCode() == 404) {
                JOptionPane.showMessageDialog(null, "Nie można sporządzić wykresu z powodu braku danych!");

            }

        } catch (ConnectException e) {

            System.out.println("Nie moge polaczyc sie z serwerem");


        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();
        }
    }
    public static void postBmiData() {
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/bmi");
        Gson gson= new Gson();
        final UserData userData = new UserData(firstName,lastName,height,weight);

        final String json=gson.toJson(userData);


        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);
            final HttpEntity httpEntity = response.getEntity();
            if(response.getStatusLine().getStatusCode()==201){
                JOptionPane.showMessageDialog(null, "BMI zostało wyliczone, a dane w pliku zostały zapisane.");

            }
            else if(response.getStatusLine().getStatusCode()==400){
                JOptionPane.showMessageDialog(null, "Problem  z wyliczeniem współczynnika BMI.");
            }
            client.close();

        } catch (ConnectException e) {

            System.out.println("Nie moge polaczyc sie z serwerem");
            System.out.println();

        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }


    }


    }
