package com.example.demo;

import bmiclient.Main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BMI_GUI {
    private JPanel newClient_BMI;
    private JPanel westLabelPanel;
    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel heightLabel;
    private JLabel weightLabel;
    private JPanel eastTextField;
    private JTextField firstNameTextField;
    private JTextField lastNameTextField;
    private JTextField heightTextField;
    private JTextField weightTextField;
    private JPanel southButtons;
    private JButton sendDataButton;
    private JButton showPieButton;
    private JButton clearDataButton;

    public BMI_GUI() {
        sendDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.firstName=firstNameTextField.getText();
                Main.lastName=lastNameTextField.getText();
                Main.height=heightTextField.getText();
                Main.weight=weightTextField.getText();
                Main.postBmiData();
            }
        });
        showPieButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Main.getServerBmiResponse();
            }
        });
        clearDataButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstNameTextField.setText("");
                lastNameTextField.setText("");
                heightTextField.setText("");
                weightTextField.setText("");
            }
        });
    }

    public static void main(String[] args) {

    JFrame frame = new JFrame();

        frame.setContentPane(new BMI_GUI().newClient_BMI);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }
}
